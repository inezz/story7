from django.test import TestCase
from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.http import HttpRequest
from .models import Status
from .forms import StatusForm
# from selenium import webdriver
from django.apps import apps
from .apps import StatusConfig
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
import time


class story6UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_can_create_status(self):
        new_status = Status.objects.create(status = 'coba pertama')
        self.assertTrue(isinstance(new_status, Status))
        self.assertTrue(new_status.__str__(), new_status.status)
        available_status = Status.objects.all().count()
        self.assertEqual(available_status,1)
    
    # def test_show_status(self):
    #     status = "1st try"
    #     data = {'message' : status}
    #     post_data = Client().post('/', data)
    #     self.assertEqual(post_data.status_code, 200)

    # def test_greeting_exists_2(self):
    #     response = Client().get('/')
    #     response_content = response.content.decode('utf-8')
    #     self.assertIn("How are you feeling today?,", response_content)

    def test_form_validation_for_filled_items(self) :
        response = self.client.post('', data={'status' : 'Status'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')



# Create your tests here.
