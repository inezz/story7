from django.test import TestCase
from django.test.client import Client


class story6UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/challenge/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used(self):
        response = Client().get('/challenge/')
        self.assertTemplateUsed(response, 'challenge.html')

    def test_npm(self):
        response = Client().get('/challenge/')
        response_content = response.content.decode('utf-8')
        self.assertIn("1806241091", response_content)



# Create your tests here.
